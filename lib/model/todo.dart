class Todo {
  final id;
  final groupId;
  final todoText;
  late bool isDone;

  Todo({required this.id, required this.groupId, required this.todoText});
}
