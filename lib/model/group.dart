class Group {
  final String id;
  final String name;

  const Group({required this.id, required this.name});
}