// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo.dart';

// **************************************************************************
// RealmObjectGenerator
// **************************************************************************

// ignore_for_file: type=lint
class Todo extends _Todo with RealmEntity, RealmObjectBase, RealmObject {
  Todo(
    String id,
    String groupId,
    String todoText,
    bool isDone,
  ) {
    RealmObjectBase.set(this, 'id', id);
    RealmObjectBase.set(this, 'groupId', groupId);
    RealmObjectBase.set(this, 'todoText', todoText);
    RealmObjectBase.set(this, 'isDone', isDone);
  }

  Todo._();

  @override
  String get id => RealmObjectBase.get<String>(this, 'id') as String;
  @override
  set id(String value) => RealmObjectBase.set(this, 'id', value);

  @override
  String get groupId => RealmObjectBase.get<String>(this, 'groupId') as String;
  @override
  set groupId(String value) => RealmObjectBase.set(this, 'groupId', value);

  @override
  String get todoText =>
      RealmObjectBase.get<String>(this, 'todoText') as String;
  @override
  set todoText(String value) => RealmObjectBase.set(this, 'todoText', value);

  @override
  bool get isDone => RealmObjectBase.get<bool>(this, 'isDone') as bool;
  @override
  set isDone(bool value) => RealmObjectBase.set(this, 'isDone', value);

  @override
  Stream<RealmObjectChanges<Todo>> get changes =>
      RealmObjectBase.getChanges<Todo>(this);

  @override
  Todo freeze() => RealmObjectBase.freezeObject<Todo>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObjectBase.registerFactory(Todo._);
    return const SchemaObject(ObjectType.realmObject, Todo, 'Todo', [
      SchemaProperty('id', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('groupId', RealmPropertyType.string),
      SchemaProperty('todoText', RealmPropertyType.string),
      SchemaProperty('isDone', RealmPropertyType.bool),
    ]);
  }
}
