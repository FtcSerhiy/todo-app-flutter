
import 'package:flutter/material.dart';
import 'package:todolist/feautures/widgets/button.dart';
import 'package:todolist/feautures/widgets/input.dart';
import 'package:todolist/feautures/widgets/todo_item.dart';
import 'package:todolist/model/model.dart';

class TodoLists extends StatefulWidget {
  TodoLists({Key? key}) : super(key: key);

  @override
  State<TodoLists> createState() => _TodoListsState();
}

class _TodoListsState extends State<TodoLists> {
  static Group group = Group(id: DateTime.now().toString(), name: 'Start');
  final todoList = [Todo(id: DateTime.now().toString(), todoText: 'create todo lists', groupId: group.id,)];
  final _todoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text(group.name),),
          backgroundColor: Colors.grey,
          body: Stack(children: [
            ListView(
              padding:
                  EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50, bottom: 20),
                  child: Text(
                    'All todos',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                  ),
                ),
                for (Todo todo in todoList)
                  TodoItem(
                      todo: todo,
                      onDeleteItem: _deleteTodoItem,
                      onTodoChanged: _handleTodoChange)
              ],
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: Row(
                  children: [
                    Input(controller: _todoController),
                    Button(controller: _todoController, add: this._addTodoItem),
                  ],
                ))
          ])),
    );
  }

  void _handleTodoChange(Todo todo) {
    setState(() {
      todo.isDone = !todo.isDone;
    });
  }

  void _deleteTodoItem(String id) {
    setState(() {
      todoList.removeWhere((element) => element.id == id);
    });
  }

  void _addTodoItem(String todo) {
    setState(() {
      todoList.add(Todo(id: DateTime.now().toString(), groupId: group.id, todoText: 'Created '));
      _todoController.clear();
    });
  }
}
