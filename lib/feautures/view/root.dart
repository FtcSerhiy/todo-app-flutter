import 'package:flutter/material.dart';
import 'package:todolist/feautures/widgets/button.dart';
import 'package:todolist/feautures/widgets/group_item.dart';
import 'package:todolist/feautures/widgets/input.dart';
import 'package:todolist/model/group.dart';

class Groups extends StatefulWidget {
  Groups({Key? key}) : super(key: key);

  @override
  State<Groups> createState() => _GroupsState();
}

class _GroupsState extends State<Groups> {
  final _groupController = TextEditingController();
  final groups = [Group(id: DateTime.now().toString(), name: 'Programming')];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Your groups'),),
          backgroundColor: Colors.grey,
          body: Stack(children: [
            ListView(
              padding:
                  EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50, bottom: 20),
                  child: Text(
                    'All todos',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                  ),
                ),
                for (Group group in groups)
                  GroupItem(
                      group: group,
                      onDeleteItem: _deleteGroupItem)
              ],
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: Row(
                  children: [
                    Input(controller: _groupController),
                    Button(controller: _groupController, add: this._addGroupItem),
                  ],
                ))
          ])),
    );
  }

  void _deleteGroupItem(String id) {
    setState(() {
      groups.removeWhere((element) => element.id == id);
    });
  }

  void _addGroupItem(String name) {
    setState(() {
      groups.add(Group(id: DateTime.now().toString(), name: name));
      _groupController.clear();
    });
  }
}
