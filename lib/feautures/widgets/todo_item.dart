import 'package:flutter/material.dart';
import 'package:todolist/model/todo.dart';

class TodoItem extends StatelessWidget {
  const TodoItem(
      {Key? key,
      required this.todo,
      required this.onDeleteItem,
      required this.onTodoChanged})
      : super(key: key);
  final Todo todo;
  final onDeleteItem;
  final onTodoChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: ListTile(
          onTap: () {
            onTodoChanged(todo);
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          tileColor: Colors.white,
          leading: Icon(
              todo.isDone ? Icons.check_box : Icons.check_box_outline_blank,
              color: Colors.blueAccent),
          title: Text(
            todo.todoText,
            style: TextStyle(
                fontSize: 16,
                color: Colors.black,
                decoration: todo.isDone ? TextDecoration.lineThrough : null),
          ),
          trailing: Container(
            padding: EdgeInsets.all(0),
            margin: EdgeInsets.symmetric(
              horizontal: 12,
            ),
            height: 35,
            width: 35,
            decoration: BoxDecoration(
                color: Colors.redAccent,
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: IconButton(
              color: Colors.white,
              onPressed: () {
                onDeleteItem(this.todo.id);
              },
              iconSize: 18,
              icon: Icon(Icons.delete),
              hoverColor: Colors.red,
            ),
          ),
        ));
  }
}
