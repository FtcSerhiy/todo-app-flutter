import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({super.key, required this.controller, required this.add});
  final controller;
  final add;

  @override
  Widget build(BuildContext context) {
    return Container(
                      margin: EdgeInsets.only(bottom: 20, right: 20),
                      child: ElevatedButton(
                        child: Text(
                          '+',
                          style: TextStyle(fontSize: 40),
                        ),
                        onPressed: () {
                          add(controller.text);
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.blue,
                            foregroundColor: Colors.white,
                            minimumSize: Size(60, 60),
                            elevation: 10),
                      ),
                    );
  }
}