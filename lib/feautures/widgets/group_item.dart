import 'package:flutter/material.dart';
import 'package:todolist/model/group.dart';

class GroupItem extends StatelessWidget {
  const GroupItem(
      {Key? key,
      required this.group,
      required this.onDeleteItem})
      : super(key: key);
  final Group group;
  final onDeleteItem;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/lists', arguments: group.id);
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          tileColor: Colors.white,
          title: Text(
            group.name,
            style: TextStyle(
                fontSize: 16,
                color: Colors.black),
          ),
          trailing: Container(
            padding: EdgeInsets.all(0),
            margin: EdgeInsets.symmetric(
              horizontal: 12,
            ),
            height: 35,
            width: 35,
            decoration: BoxDecoration(
                color: Colors.redAccent,
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: IconButton(
              color: Colors.white,
              onPressed: () {
                onDeleteItem(this.group.id);
              },
              iconSize: 18,
              icon: Icon(Icons.delete),
              hoverColor: Colors.red,
            ),
          ),
        ));
  }
}
