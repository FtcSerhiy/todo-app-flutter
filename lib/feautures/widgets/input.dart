import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  const Input({super.key, required this.controller});
  final controller;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
    margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
    padding:
        EdgeInsets.symmetric(horizontal: 20, vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 0.0),
              blurRadius: 10.0,
              spreadRadius: 0.0)
        ],
        borderRadius: BorderRadius.circular(10)),
    child: TextField(
        controller: controller,
        decoration: InputDecoration(
            hintText: 'Add a new todo item',
            border: InputBorder.none)),
  ));
  }
}