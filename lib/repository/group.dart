import 'package:realm/realm.dart';
import 'package:todolist/model/group.dart';
import 'package:todolist/model/todo.dart';

class Groups {
  final Realm realm;
  const Groups({required this.realm});

  String createGroup(Group group) {
    return realm.write(() => realm.add<Group>(group).toString());
  }

  List<Todo> getAllTodoItems(String id) {
    return realm.all<Todo>().query(r'group_id == $0', [id]).toList();
  }

  void deleteGroup(Group group) {
    realm.write(() => realm.delete<Group>(group));
  }
}
