import 'package:realm/realm.dart';
import 'package:todolist/model/todo.dart';

class TodoItem {
  final Realm realm;

  const TodoItem({required this.realm});
  List<Todo> getAllByGroupId(String id) {
    return realm.all<Todo>().query(r'id == $0', [id]).toList();
  }
  void createItem(Todo todo) {
    realm.write(() => realm.add(todo));
  }

  void done(Todo todo) {
    realm.write(() => todo.isDone = true);
  }

  void deleteItem(String id) {
    final todo = realm.all<Todo>().query(r'id == $0', [id]).single;
    realm.delete(todo);
  }
}