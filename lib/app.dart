import 'package:flutter/material.dart';
import 'package:todolist/feautures/view/view.dart';
import 'package:todolist/theme/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Todo App',
      home: TodoLists(),
      theme: darkTheme,
    );
  }
}
