import 'package:flutter/material.dart';

final darkTheme = ThemeData(
  scaffoldBackgroundColor: Colors.black,
  primarySwatch: Colors.red,
  appBarTheme: AppBarTheme(
    backgroundColor: Colors.black12,
    iconTheme: IconThemeData(color: Colors.white),
  )
);